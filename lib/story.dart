class Story {
  String storyTitle, choice1, choice2;
  int? option1, option2;

  Story(
      {required this.storyTitle,
      required this.choice1,
      required this.choice2,
      this.option1,
      this.option2});
}
